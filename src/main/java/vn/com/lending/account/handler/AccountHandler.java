package vn.com.lending.account.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import vn.com.lending.account.model.Account;
import vn.com.lending.account.repository.AccountRepository;
import vn.com.lending.publics.branch.event.CreateBranchEvent;

import java.sql.Timestamp;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Service
public class AccountHandler {
    private static AtomicInteger ID_GENERATOR = new AtomicInteger(0);

    @Autowired
    private AccountRepository accountRepository;

    @KafkaListener(topics = "vn.com.lending.branch.event", groupId = "account_group_1",containerFactory = "branchEventConcurrentKafkaListenerContainerFactory")
    public void handlerBranchEvent(CreateBranchEvent createBranchEvent){
        log.debug("Create branch event handle : [{}]",createBranchEvent);
        Account account = new Account();
        int max = 0;
        if(accountRepository.count() > 0){
            max = accountRepository.selectMax()  + 1;
        }
        account.setAccountId("CD"+ max );
        account.setBranchId(createBranchEvent.getBranchId());
        account.setName(createBranchEvent.getName());
        account.setAmount(0.0);
        account.setType(1);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        account.setCreateTime(timestamp);
        account.setUpdateTime(timestamp);
        accountRepository.save(account);

    }
}
