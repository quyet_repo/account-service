package vn.com.lending.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.com.lending.account.model.Account;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account,String> {
    @Query(value = "select MAX(id) from Account")
    public int selectMax();
}
