package vn.com.lending.account.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "history")
@Getter
@Setter
public class AppConfig {

    private List<String> transferServicesView;

    private String vpbankViewName = "VPBank";

    private int maxOldHistoryDays = 90;

    private int maxPageSize = 100;
}
